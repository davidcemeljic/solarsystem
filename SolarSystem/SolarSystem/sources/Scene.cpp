﻿#include "Scene.hpp"

void Scene::tick(double deltaTime) { for (const auto& object : objects) { object->tick(deltaTime); } }

void Scene::addObject(Object* object) { objects.emplace_back(object); }
void Scene::addLight(Light* light) { lights.emplace_back(light); }

Scene::~Scene() {
    for (auto object : objects)
        delete object;
}
