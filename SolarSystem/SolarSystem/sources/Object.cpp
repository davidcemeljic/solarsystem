﻿#include "Object.hpp"

void Object::draw(Camera* camera, std::vector<Light*>& lights) { draw(camera, static_cast<Transform>(*this), lights); }

void Object::draw(Camera* camera, Transform instanceTransform, std::vector<Light*>& lights) {
    glUseProgram(shader->ID);

    mesh->applyTransform(camera, instanceTransform, shader);

    if (!lights.empty()) {
        glUniform3fv(glGetUniformLocation(shader->ID, "lightPosition"), 1, &lights[0]->Location[0]);
        glUniform3fv(glGetUniformLocation(shader->ID, "lightColor"), 1, &lights[0]->getColor()[0]);
        glUniform1f(glGetUniformLocation(shader->ID, "lightIntensity"), lights[0]->getIntensity());
    }

    material->applyTextures();

    glBindVertexArray(mesh->VAO);
    glDrawElements(GL_TRIANGLES, mesh->indices.size() * sizeof(unsigned int), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}
