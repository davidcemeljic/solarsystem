﻿#include "Curve.hpp"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <assimp/scene.h>
#include <iostream>
#include <vector>
#include <functional>


Curve::Curve(const std::vector<glm::vec3>& control_points, Shader* const lineShader, Shader* const approxShader,
             Shader* const interpShader) {
    this->controlPoints = control_points;
    this->lineShader = lineShader;
    this->approxShader = approxShader;
    this->interpShader = interpShader;

    interpPoints = std::vector<glm::vec3>(precision);

    initControlPolygon();
    initApproxCurve();
    initInterpCurve();
}

void Curve::draw(Camera* camera) {
    drawControlPolygon(camera);
    drawApproxCurve(camera);
    drawInterpCurve(camera);
}

void Curve::addControlPoint(glm::vec3 point) {
    controlPoints.emplace_back(point);

    glBindVertexArray(VAO_ControlPolygon);

    glBindBuffer(GL_ARRAY_BUFFER, VBO_ControlPolygon);
    glBufferData(GL_ARRAY_BUFFER, controlPoints.size() * sizeof(glm::vec3), &controlPoints.front(),
                 GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindVertexArray(0);

    calculateApproxCurve();
    calculateInterpCurve();
}

void Curve::initControlPolygon() {
    glGenVertexArrays(1, &VAO_ControlPolygon);
    glGenBuffers(1, &VBO_ControlPolygon);

    glBindVertexArray(VAO_ControlPolygon);

    glBindBuffer(GL_ARRAY_BUFFER, VBO_ControlPolygon);
    glBufferData(GL_ARRAY_BUFFER, controlPoints.size() * sizeof(glm::vec3), &controlPoints.front(),
                 GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
}

void Curve::initApproxCurve() {
    glGenVertexArrays(1, &VAO_ApproxCurve);
    glGenBuffers(1, &VBO_ApproxCurve);

    glBindVertexArray(VAO_ApproxCurve);

    glBindBuffer(GL_ARRAY_BUFFER, VBO_ApproxCurve);
    glBufferData(GL_ARRAY_BUFFER, controlPoints.size() * sizeof(glm::vec3), &controlPoints.front(),
                 GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
}

void Curve::initInterpCurve() {
    glGenVertexArrays(1, &VAO_InterpCurve);
    glGenBuffers(1, &VBO_InterpCurve);

    glBindVertexArray(VAO_InterpCurve);

    glBindBuffer(GL_ARRAY_BUFFER, VBO_InterpCurve);
    glBufferData(GL_ARRAY_BUFFER, controlPoints.size() * sizeof(glm::vec3), &controlPoints.front(),
                 GL_DYNAMIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
}

void Curve::drawControlPolygon(Camera* camera) {
    glBindVertexArray(VAO_ControlPolygon);
    glUseProgram(lineShader->ID);

    glm::mat4 view = camera->getViewMatrix();
    glm::mat4 projection = camera->getPerspectiveMatrix();

    glUniformMatrix4fv(glGetUniformLocation(lineShader->ID, "view"), 1, GL_FALSE, &view[0][0]);
    glUniformMatrix4fv(glGetUniformLocation(lineShader->ID, "projection"), 1, GL_FALSE, &projection[0][0]);

    glDrawArrays(GL_LINE_STRIP, 0, controlPoints.size());

    glBindVertexArray(0);
}

void Curve::drawApproxCurve(Camera* camera) {
    if (controlPoints.size() < 2) return;

    glBindVertexArray(VAO_ApproxCurve);
    glUseProgram(approxShader->ID);

    glm::mat4 view = camera->getViewMatrix();
    glm::mat4 projection = camera->getPerspectiveMatrix();

    glUniformMatrix4fv(glGetUniformLocation(approxShader->ID, "view"), 1, GL_FALSE, &view[0][0]);
    glUniformMatrix4fv(glGetUniformLocation(approxShader->ID, "projection"), 1, GL_FALSE, &projection[0][0]);

    glDrawArrays(GL_LINE_STRIP, 0, approxPoints.size());

    glBindVertexArray(0);
}

void Curve::drawInterpCurve(Camera* camera) {
    if (controlPoints.size() < 2) return;

    glBindVertexArray(VAO_InterpCurve);
    glUseProgram(interpShader->ID);

    glm::mat4 view = camera->getViewMatrix();
    glm::mat4 projection = camera->getPerspectiveMatrix();

    glUniformMatrix4fv(glGetUniformLocation(interpShader->ID, "view"), 1, GL_FALSE, &view[0][0]);
    glUniformMatrix4fv(glGetUniformLocation(interpShader->ID, "projection"), 1, GL_FALSE, &projection[0][0]);

    glDrawArrays(GL_LINE_STRIP, 0, interpPoints.size());

    glBindVertexArray(0);
}

void Curve::calculateInterpVectors() {
    int count = std::min(static_cast<int>(controlPoints.size()), 4);
    int n = count - 1;

    glm::vec4 Ax, Ay, Az;
    glm::vec4 Px = {
        (controlPoints.end() - count)->x,
        count > 1 ? (controlPoints.end() - count + 1)->x : 0,
        count > 2 ? (controlPoints.end() - count + 2)->x : 0,
        count > 3 ? (controlPoints.end() - count + 3)->x : 0,
    };
    glm::vec4 Py = {
        (controlPoints.end() - count)->y,
        count > 1 ? (controlPoints.end() - count + 1)->y : 0,
        count > 2 ? (controlPoints.end() - count + 2)->y : 0,
        count > 3 ? (controlPoints.end() - count + 3)->y : 0,
    };
    glm::vec4 Pz = {
        (controlPoints.end() - count)->z,
        count > 1 ? (controlPoints.end() - count + 1)->z : 0,
        count > 2 ? (controlPoints.end() - count + 2)->z : 0,
        count > 3 ? (controlPoints.end() - count + 3)->z : 0,
    };

    if (n == 1) {
        glm::vec2 Px2 = glm::vec2(Px[0], Px[1]);
        glm::vec2 Py2 = glm::vec2(Py[0], Py[1]);
        glm::vec2 Pz2 = glm::vec2(Pz[0], Pz[1]);

        glm::mat2 F = {
            1, 0,
            1, f(1, n, 1.0f / n),
        };

        F = inverse(F);

        glm::vec2 Ax2 = Px2 * F;
        glm::vec2 Ay2 = Py2 * F;
        glm::vec2 Az2 = Pz2 * F;

        interpVectors = {
            glm::vec3(Ax2[0], Ay2[0], Az2[0]),
            glm::vec3(Ax2[1], Ay2[1], Az2[1]),
        };
    }
    else if (n == 2) {
        glm::vec3 Px3 = glm::vec3(Px[0], Px[1], Px[2]);
        glm::vec3 Py3 = glm::vec3(Py[0], Py[1], Py[2]);
        glm::vec3 Pz3 = glm::vec3(Pz[0], Pz[1], Pz[2]);

        glm::mat3 F = {
            1, 0, 0,
            1, f(1, n, 1.0f / n), f(2, n, 1.0f / n),
            1, f(1, n, 2.0f / n), f(2, n, 2.0f / n),
        };

        F = inverse(F);

        glm::vec3 Ax3 = Px3 * F;
        glm::vec3 Ay3 = Py3 * F;
        glm::vec3 Az3 = Pz3 * F;

        interpVectors = {
            glm::vec3(Ax3[0], Ay3[0], Az3[0]),
            glm::vec3(Ax3[1], Ay3[1], Az3[1]),
            glm::vec3(Ax3[2], Ay3[2], Az3[2])
        };
    }
    else if (n == 3) {
        glm::mat4 F = {
            1, 0, 0, 0,
            1, f(1, n, 1.0f / n), f(2, n, 1.0f / n), f(3, n, 1.0f / n),
            1, f(1, n, 2.0f / n), f(2, n, 2.0f / n), f(3, n, 2.0f / n),
            1, f(1, n, 3.0f / n), f(2, n, 3.0f / n), f(3, n, 3.0f / n)
        };
        F = inverse(F);

        Ax = Px * F;
        Ay = Py * F;
        Az = Pz * F;

        interpVectors = {
            {Ax[0], Ay[0], Az[0]},
            {Ax[1], Ay[1], Az[1]},
            {Ax[2], Ay[2], Az[2]},
            {Ax[3], Ay[3], Az[3]},
        };
    }
}

glm::vec3 Curve::getApproxPoint(float t) {
    glm::vec3 point = {0, 0, 0};
    for (int i = 0; i < controlPoints.size(); i++) { point += b(i, controlPoints.size() - 1, t) * controlPoints[i]; }
    return point;
}

glm::vec3 Curve::getInterpPoint(float t) {
    int n = std::min(static_cast<int>(controlPoints.size()), 4) - 1;

    glm::vec3 point = glm::vec3(0, 0, 0);
    for (auto i = 0; i < interpVectors.size(); i++)
        point += interpVectors[i] * f(i, n, t);

    return point;
}

glm::vec3 Curve::getInterpTangent(float t) { return getInterpPoint(t + 0.01f) - getInterpPoint(t - 0.01f); }

void Curve::calculateApproxCurve() {
    approxPoints.clear();
    int pointCount = precision * (controlPoints.size() - 1);
    for (int i = 0; i < pointCount; i++)
        approxPoints.emplace_back(getApproxPoint(static_cast<float>(i) / static_cast<float>(pointCount - 1)));

    glBindVertexArray(VAO_ApproxCurve);
    glUseProgram(approxShader->ID);

    glBindBuffer(GL_ARRAY_BUFFER, VBO_ApproxCurve);

    glBufferData(GL_ARRAY_BUFFER, approxPoints.size() * sizeof(glm::vec3),
                 &approxPoints.front(), GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
}

void Curve::calculateInterpCurve() {
    calculateInterpVectors();
    interpPoints.clear();
    int pointCount = precision * (controlPoints.size() - 1);
    for (int i = 0; i < pointCount; i++)
        interpPoints.emplace_back(getInterpPoint(static_cast<float>(i) / static_cast<float>(pointCount - 1)));

    glBindVertexArray(VAO_InterpCurve);
    glUseProgram(interpShader->ID);

    glBindBuffer(GL_ARRAY_BUFFER, VBO_InterpCurve);

    glBufferData(GL_ARRAY_BUFFER, interpPoints.size() * sizeof(glm::vec3),
                 &interpPoints.front(), GL_DYNAMIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
}

float Curve::f(int i, int n, float t) {
    //if (i > n) return 0.0f;

    float result = 0.0f;
    for (int j = i; j <= n; j++) result += b(j, n, t);

    return result;
}

float Curve::b(int i, int n, float t) { return binomialCoeff(n, i) * pow(t, i) * pow(1 - t, n - i); }

int Curve::binomialCoeff(int n, int k) {
    if (k == 0 || n == k) return 1;

    return fact(n) / (fact(k) * fact(n - k));
}

int Curve::fact(int n) {
    int result = 1;
    for (int i = 2; i <= n; i++)
        result *= i;

    return result;
}
