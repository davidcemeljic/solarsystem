﻿#include "Planet.hpp"

#include "globals.hpp"

void Planet::tick(float deltaTime) {
    if (revolutionTime > 0.0001f) rotateRelative({0, deltaTime * speed / revolutionTime, 0});
    if (rotationTime > 0.0001f) rotate({0, deltaTime * speed / rotationTime, 0});
}
