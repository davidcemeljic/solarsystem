#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>

// Standard Headers
#include <iostream>
#include <cstdlib>
#include <functional>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "globals.hpp"
#include "Curve.hpp"
#include "Planet.hpp"

int width = 1280, height = 800;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

bool cameraControls = true;
bool lineRenderingMode = true;

GLint cameraPosInUniformLocation;
GLint selectedUniformLocation;

GLint projectionUniformLocation;
GLint viewUniformLocation;
GLint modelUniformLocation;
GLint normalMatrixUniformLocation;

Renderer* renderer;
Camera* activeCamera;

float speed = 1.0f;

int currentObjectIndex = 0;

float lastX = width / 2.0f;
float lastY = height / 2.0f;
bool firstMouse = true;

std::string defaultPath;

Shader* loadShader(char* path, char* naziv) {
    std::string sPath(path);
    std::string pathVert;
    std::string pathFrag;
    std::string pathGeom;

    pathVert.append(path, sPath.find_last_of("\\/") + 1);
    pathFrag.append(path, sPath.find_last_of("\\/") + 1);
    pathGeom.append(path, sPath.find_last_of("\\/") + 1);
    if (pathFrag[pathFrag.size() - 1] == '/') {
        pathVert.append("shaders/");
        pathFrag.append("shaders/");
        pathGeom.append("shaders/");
    }
    else if (pathFrag[pathFrag.size() - 1] == '\\') {
        pathVert.append("shaders\\");
        pathFrag.append("shaders\\");
        pathGeom.append("shaders\\");
    }
    else {
        std::cerr << "nepoznat format pozicije shadera";
        exit(1);
    }

    pathVert.append(naziv);
    pathVert.append(".vert");
    pathFrag.append(naziv);
    pathFrag.append(".frag");
    pathGeom.append(naziv);
    pathGeom.append(".geom");

    return new Shader(pathVert.c_str(), pathFrag.c_str(), pathGeom.c_str());
}

std::string getResourcePath(std::string resourceName) {
    std::string fullName;
    fullName.append(defaultPath, 0, defaultPath.find_last_of("\\/") + 1);
    if (fullName[fullName.size() - 1] == '/')
        fullName.append("resources/");
    else if (fullName[fullName.size() - 1] == '\\')
        fullName.append("resources\\");

    fullName.append(resourceName);
    return fullName;
}

void framebuffer_size_callback(GLFWwindow* window, int Width, int Height) {
    width = Width;
    height = Height;

    if (activeCamera != nullptr) {
        activeCamera->width = width;
        activeCamera->height = height;
    }

    glViewport(0, 0, width, height);
}

struct Vertex {
    GLfloat position[3] = {0, 0, 0};
    GLubyte colour[4] = {0, 0, 0, 1};

    Vertex() = default;

    Vertex(const float x, const float y, const float z) {
        position[0] = x;
        position[1] = y;
        position[2] = z;
    }
};


void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) glfwSetWindowShouldClose(window, GL_TRUE);
    if (key == GLFW_KEY_UP && action == GLFW_PRESS) speed+=1.0f;
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) { }

void processInput(GLFWwindow* window) {
    float cameraSpeed = 2.5f * deltaTime;
    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)) cameraSpeed *= 5.0f;

    if (glfwGetKey(window, GLFW_KEY_W)) activeCamera->move(activeCamera->front * cameraSpeed);
    if (glfwGetKey(window, GLFW_KEY_S)) activeCamera->move(-activeCamera->front * cameraSpeed);
    if (glfwGetKey(window, GLFW_KEY_A)) activeCamera->move(-activeCamera->right * cameraSpeed);
    if (glfwGetKey(window, GLFW_KEY_D)) activeCamera->move(activeCamera->right * cameraSpeed);
    if (glfwGetKey(window, GLFW_KEY_Q)) activeCamera->move(activeCamera->viewUp * cameraSpeed);
    if (glfwGetKey(window, GLFW_KEY_E)) activeCamera->move(-activeCamera->viewUp * cameraSpeed);

}

void mouse_callback(GLFWwindow* window, double x, double y) {
    if (firstMouse) {
        lastX = x;
        lastY = y;
        firstMouse = false;
    }

    float xoffset = x - lastX;
    float yoffset = lastY - y;

    lastX = x;
    lastY = y;

    float sensitivity = 0.005f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    activeCamera->rotate(glm::vec3(0, yoffset, xoffset));
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) { }

int main(int argc, char* argv[]) {
    GLFWwindow* window;

    glfwInit();
    gladLoadGL();

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);

    window = glfwCreateWindow(width, height, "Solar System", nullptr, nullptr);
    if (window == nullptr) {
        fprintf(stderr, "Failed to Create OpenGL Context");
        exit(EXIT_FAILURE);
    }
    glfwMakeContextCurrent(window);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        fprintf(stderr, "Failed to initialize GLAD");
        exit(-1);
    }
    fprintf(stderr, "OpenGL %s\n", glGetString(GL_VERSION));


    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glClearColor(0, 0, 0, 1);

    glfwSwapInterval(0);

    Assimp::Importer importer;

    defaultPath = argv[0];

    Shader* planetShader = loadShader(argv[0], "phongShading");
    Shader* sunShader = loadShader(argv[0], "phongShadingUnlit");

    activeCamera = new Camera(glm::vec3(3, 4, 1), glm::vec3(0, 0, 0), width, height);

    auto scene = new Scene(activeCamera);
    auto sun = scene->load<Planet>(getResourcePath("sun.obj"), sunShader);
    auto earth = scene->load<Planet>(getResourcePath("earth.obj"), planetShader);
    auto moon = scene->load<Planet>(getResourcePath("moon.obj"), planetShader);

    earth->attachTo(sun);
    earth->setLocation({3, 0, 0});
    earth->setScale({0.25, 0.25, 0.25});

    moon->attachTo(earth);
    moon->setLocation({0.75, 0, 0});
    moon->setScale({0.1, 0.1, 0.1});


    sun->rotationTime = 27.0f;
    sun->revolutionTime = 0.0f;

    earth->revolutionTime = 10.0f;

    renderer = new Renderer(scene, activeCamera);

    scene->addLight(new Light({0, 0, 0}, {1, 1, 0.8}, 1));


    while (glfwWindowShouldClose(window) == false) {
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        scene->tick(deltaTime);

        renderer->render();

        glfwSwapBuffers(window);
        glfwPollEvents();
        processInput(window);
    }

    delete renderer;

    glfwTerminate();

    return EXIT_SUCCESS;
}
