﻿#include "Renderer.hpp"
#include "Camera.hpp"

Renderer::~Renderer() { delete camera; }

Renderer::Renderer(Scene* scene, Camera* camera) {
    this->scene = scene;
    this->camera = camera;
}

void Renderer::render() const {
    for (const auto object : scene->objects) object->draw(camera, scene->lights);
}
