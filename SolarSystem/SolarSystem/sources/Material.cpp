﻿#include "Material.h"

#include <iostream>
#include <regex>
#include <stb_image.h>

#include "Shader.h"

void Material::setup(Shader* shader) {
    glUseProgram(shader->ID);

    glUniform3fv(glGetUniformLocation(shader->ID, "ambientColor"), 1, &getAmbientColor()[0]);
    glUniform3fv(glGetUniformLocation(shader->ID, "diffuseColor"), 1, &getDiffuseColor()[0]);
    glUniform3fv(glGetUniformLocation(shader->ID, "specularColor"), 1, &getSpecularColor()[0]);
    glUniform3fv(glGetUniformLocation(shader->ID, "emissiveColor"), 1, &getEmissiveColor()[0]);

    glUniform1f(glGetUniformLocation(shader->ID, "shininess"), getShininess());
    glUniform1f(glGetUniformLocation(shader->ID, "opacity"), getOpacity());

    for (auto& texture : textures) generateTexture(shader, texture);
}

std::string Material::getName() const {
    std::string name;
    if (material->Get(AI_MATKEY_NAME, name) == AI_SUCCESS)
        return name;

    return "";
}

glm::vec3 Material::getAmbientColor() const {
    aiColor3D color;
    if (material->Get(AI_MATKEY_COLOR_AMBIENT, color) == AI_SUCCESS)
        return {color.r, color.g, color.b};

    return {0, 0, 0};
}

glm::vec3 Material::getDiffuseColor() const {
    aiColor3D color;
    if (material->Get(AI_MATKEY_COLOR_DIFFUSE, color) == AI_SUCCESS)
        return {color.r, color.g, color.b};

    return {0, 0, 0};
}

glm::vec3 Material::getSpecularColor() const {
    aiColor3D color;
    if (material->Get(AI_MATKEY_COLOR_SPECULAR, color) == AI_SUCCESS)
        return {color.r, color.g, color.b};

    return {0, 0, 0};
}

glm::vec3 Material::getEmissiveColor() const {
    aiColor3D color;
    if (material->Get(AI_MATKEY_COLOR_EMISSIVE, color) == AI_SUCCESS)
        return {color.r, color.g, color.b};

    return {0, 0, 0};
}

float Material::getShininess() const {
    float shininess;
    if (material->Get(AI_MATKEY_SHININESS, shininess) == AI_SUCCESS)
        return shininess;

    return 0;
}

float Material::getOpacity() const {
    float opacity;
    if (material->Get(AI_MATKEY_OPACITY, opacity) == AI_SUCCESS)
        return opacity;

    return 1;
}

void Material::loadTextures(const std::string& texturePath) {
    aiString texturePosition;
    if (material->Get(AI_MATKEY_TEXTURE(aiTextureType_DIFFUSE, 0), texturePosition) == AI_SUCCESS) {
        const std::string textureName = texturePath + texturePosition.C_Str();
        std::cout << "Loading texture: " << textureName << std::endl;

        Texture2D tex;
        tex.data = stbi_load(textureName.c_str(), &tex.width, &tex.height, &tex.channels, 0);
        tex.type = aiTextureType_DIFFUSE;
        if (tex.data) textures.emplace_back(tex);
    }
    if (material->Get(AI_MATKEY_TEXTURE(aiTextureType_SPECULAR, 0), texturePosition) == AI_SUCCESS) {
        const std::string textureName = texturePath + texturePosition.C_Str();
        std::cout << "Loading texture: " << textureName << std::endl;

        Texture2D tex;
        tex.data = stbi_load(textureName.c_str(), &tex.width, &tex.height, &tex.channels, 0);
        tex.type = aiTextureType_SPECULAR;
        if (tex.data) textures.emplace_back(tex);
    }
}


void Material::generateTexture(Shader* shader, Texture2D& texture) {
    glGenTextures(1, &texture.ID);

    switch (texture.type) {
    case aiTextureType_DIFFUSE: {
        std::cout << "Generating texture: " << texture.ID << " (diffuse)" << std::endl;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture.ID);
        glUniform1i(glGetUniformLocation(shader->ID, "diffuseMap"), 0);
        glUniform1i(glGetUniformLocation(shader->ID, "useDiffuseMap"), 1);
        break;
    }
    case aiTextureType_SPECULAR: {
        std::cout << "Generating texture: " << texture.ID << " (specular)" << std::endl;
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, texture.ID);
        glUniform1i(glGetUniformLocation(shader->ID, "specularMap"), 1);
        glUniform1i(glGetUniformLocation(shader->ID, "useSpecularMap"), 1);
        break;
    }
    case aiTextureType_EMISSIVE: {
        std::cout << "Generating texture: " << texture.ID << " (emissive)" << std::endl;
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, texture.ID);
        glUniform1i(glGetUniformLocation(shader->ID, "emissiveMap"), 2);
        glUniform1i(glGetUniformLocation(shader->ID, "useEmissiveMap"), 1);
        break;
    }
    default: ;
    }

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture.width, texture.height, 0, GL_RGB, GL_UNSIGNED_BYTE, texture.data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

}

void Material::applyTextures() {
    for (int i = 0; i < textures.size(); i++) {
        glActiveTexture(GL_TEXTURE0 + i);
        glBindTexture(GL_TEXTURE_2D, textures[i].ID);
    }
}
