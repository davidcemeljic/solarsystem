﻿#include "Camera.hpp"

#include <iostream>
#include <glm/gtx/vector_angle.hpp>

#include "GLFW/glfw3.h"

Camera::Camera(glm::vec3 eyeLocation, glm::vec3 center, int width, int height) {
    Location = eyeLocation;
    this->center = center;
    this->width = width;
    this->height = height;

    front = center - eyeLocation;
    focalDistance = length(front);

    front = normalize(front);
    right = normalize(cross(front, worldUp));
    viewUp = normalize(cross(right, front));
}

void Camera::rotate(glm::vec3 rot) {
    glm::mat4 rM = glm::mat4(1.0f);
    rM = glm::rotate(rM, -rot.z, glm::vec3(0, 1, 0));
    front = glm::vec3(rM * glm::vec4(front, 1.0f));
    right = glm::vec3(rM * glm::vec4(right, 1.0f));
    viewUp = glm::vec3(rM * glm::vec4(viewUp, 1.0f));

    rM = glm::mat4(1.0f);
    rM = glm::rotate(rM, rot.y, right);
    glm::vec3 newFront = glm::vec3(rM * glm::vec4(front, 1.0f));
    glm::vec3 newViewUp = glm::vec3(rM * glm::vec4(viewUp, 1.0f));

    float angle = glm::degrees(glm::angle(newFront, glm::vec3(newFront.x, 0, newFront.z)));
    if (angle < 89.0f) {
        front = newFront;
        viewUp = newViewUp;
    }
}

void Camera::setForwardVector(glm::vec3 forward) {
    front = normalize(forward);
    right = normalize(cross(front, worldUp));
    viewUp = normalize(cross(right, front));
}

void Camera::startAnimation(Curve* path, float time) {
    this->path = path;
    this->animTime = time;
    this->animationStart = glfwGetTime();
}
