﻿#include "Light.h"

Light::Light(glm::vec3 position, glm::vec3 color, float intensity) {
    Location = position;
    this->color = color;
    this->intensity = intensity;
}

glm::vec3 Light::getColor() { return color; }
float Light::getIntensity() { return intensity; }

void Light::setColor(glm::vec3 newColor) { this->color = newColor; }
void Light::setIntensity(float newIntensity) { this->intensity = newIntensity; }
