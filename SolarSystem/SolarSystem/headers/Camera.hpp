﻿#pragma once

#include <functional>
#include <glm/vec3.hpp>
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_transform.hpp>

#include "Transform.hpp"

class Curve;

class Camera : public Transform {
    Curve* path = nullptr;
public:
    glm::vec3 center = glm::vec3(0, 0, 0);
    glm::vec3 worldUp = glm::vec3(0, 1, 0);
    glm::vec3 viewUp = glm::vec3(0, 1, 0);
    glm::vec3 front{};
    glm::vec3 right{};

    float fov = 45.0f;
    float nearPlane = 0.001f;
    float farPlane = 100.0f;
    float focalDistance = 5.0f;

    int width = 1280;
    int height = 720;

    double animationStart;
    float animTime;

    std::function<void()> tickFunction;

    Camera(glm::vec3 eyeLocation, glm::vec3 center, int width, int height);

    void rotate(glm::vec3 rot);

    void setForwardVector(glm::vec3 forward);

    void startAnimation(Curve* path, float time);

    void tick() { if (tickFunction) tickFunction(); }

    glm::mat4 getViewMatrix() {
        right = normalize(cross(front, viewUp));
        viewUp = normalize(cross(right, front));

        return lookAt(Location, Location + front * focalDistance, viewUp);
    }

    glm::mat4 getPerspectiveMatrix() {
        float aspectRatio = static_cast<float>(width) / static_cast<float>(height);

        return glm::perspective(glm::radians(fov), aspectRatio, nearPlane, farPlane);
    }
};
