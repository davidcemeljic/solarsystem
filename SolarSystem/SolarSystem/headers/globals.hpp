﻿#pragma once

#include <glad/glad.h>

#include "Renderer.hpp"

extern int width, height;

extern float deltaTime;
extern float lastFrame;

extern bool cameraControls;
extern bool lineRenderingMode;

extern Renderer* renderer;
extern Camera* activeCamera;

extern float speed;
