﻿#pragma once

#include <iostream>
#include <vector>

#include "Object.hpp"

#include "assimp/Importer.hpp"
#include "assimp/postprocess.h"
#include "assimp/scene.h"

class Scene {
public:
    std::vector<Object*> objects;
    std::vector<Light*> lights;

    Camera* activeCamera;

    Scene(Camera* camera) : activeCamera(camera) {}

    void tick(double deltaTime);
    
    template <class T>
    T* load(std::string path, Shader* shader, bool bLoadMaterials = true) {
        if (path.empty()) return nullptr;

        Assimp::Importer importer;

        std::cout << "Loading scene: " << path << std::endl;

        const aiScene* scene = importer.ReadFile(path.c_str(),
                                                 aiProcess_CalcTangentSpace |
                                                 aiProcess_Triangulate |
                                                 aiProcess_JoinIdenticalVertices |
                                                 aiProcess_SortByPType | aiProcess_FlipUVs);

        if (!scene)
            std::cerr << importer.GetErrorString();

        T* object = nullptr;

        for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
            Material* material;
            if (scene->HasMaterials() && bLoadMaterials)
                material = new Material(scene->mMaterials[scene->mMeshes[i]->mMaterialIndex], path);

            object = new T(new Mesh(scene->mMeshes[i]), material, shader);
            objects.emplace_back(object);
        }

        return object;
    }

    void addObject(Object* object);
    void addLight(Light* light);

    ~Scene();
};
