﻿#pragma once
#include "Object.hpp"

class Planet : public Object {
public:
    float revolutionTime = 365.25f;
    float rotationTime = 1.0f;


    Planet(Mesh* const mesh, Material* const material, Shader* const shader)
        : Object(mesh, material, shader) {}

    void tick(float deltaTime) override;

};
