﻿#pragma once

#include <glm/fwd.hpp>
#include <glm/trigonometric.hpp>
#include <glm/vec3.hpp>
#include <glm/ext/matrix_transform.hpp>

struct Transform {
    Transform* parent = nullptr;

    glm::vec3 Location = glm::vec3(0), Rotation = glm::vec3(0), Scale = glm::vec3(1);
    glm::vec3 RotationFromParent = glm::vec3(0);

    void setLocation(glm::vec3 newLocation) { Location = newLocation; }

    void setRotation(glm::vec3 newRotation) { Rotation = newRotation; }

    void setRelativeRotation(glm::vec3 newRotation) {
        RotationFromParent = newRotation;
        if (parent) {
            glm::vec3 parentWorldLocation = parent->getWorldLocation();
            
            glm::mat4 model = glm::mat4(1.0f);
            model = translate(model, parentWorldLocation);
            model = glm::rotate(model, glm::radians(RotationFromParent.x), glm::vec3(1.0f, 0.0f, 0.0f));
            model = glm::rotate(model, glm::radians(RotationFromParent.y), glm::vec3(0.0f, 1.0f, 0.0f));
            model = glm::rotate(model, glm::radians(RotationFromParent.z), glm::vec3(0.0f, 0.0f, 1.0f));
            model = translate(model, -parentWorldLocation);

            Location = glm::vec3(model * glm::vec4(parentWorldLocation + Location, 0.0f)) - parentWorldLocation;
            RotationFromParent = glm::vec3(0);
        }
    }

    void setScale(glm::vec3 newScale) { Scale = newScale; }

    void move(glm::vec3 distance) { setLocation(Location + distance); }

    void rotate(glm::vec3 angle) { setRotation(Rotation + angle); }

    void rotateRelative(glm::vec3 angle) { setRelativeRotation(RotationFromParent + angle); }

    void scale(glm::vec3 factor) { setScale(Scale * factor); }

    glm::vec3 getWorldLocation() {
        if (parent) return parent->getWorldLocation() + Location;
        return Location;
    }

    glm::mat4 getModelMatrix() {
        glm::mat4 model = glm::mat4(1.0f);

        /*if (parent) model = translate(model, parent->getWorldLocation());
        model = glm::rotate(model, glm::radians(RotationFromParent.x), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::rotate(model, glm::radians(RotationFromParent.y), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(RotationFromParent.z), glm::vec3(0.0f, 0.0f, 1.0f));
        if (parent) model = translate(model, -parent->getWorldLocation());*/

        model = translate(model, getWorldLocation());
        
        model = glm::rotate(model, glm::radians(Rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::rotate(model, glm::radians(Rotation.y), glm::vec3(0.0f, 1.0f, 0.0f));
        model = glm::rotate(model, glm::radians(Rotation.z), glm::vec3(0.0f, 0.0f, 1.0f));
        model = glm::scale(model, Scale);

        return model;
    }

    glm::mat4 getViewMatrix() { return inverse(getModelMatrix()); }

    friend bool operator==(const Transform& lhs, const Transform& rhs) {
        return lhs.Location == rhs.Location
            && lhs.Rotation == rhs.Rotation
            && lhs.Scale == rhs.Scale;
    }

    friend bool operator!=(const Transform& lhs, const Transform& rhs) { return !(lhs == rhs); }
};
