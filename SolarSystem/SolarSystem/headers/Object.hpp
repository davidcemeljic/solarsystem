﻿#pragma once
#include "Light.h"
#include "Material.h"
#include "Mesh.hpp"
#include "Shader.h"

class Object : public Transform {
public:
    Mesh* mesh;
    Material* material;
    Shader* shader;

    std::vector<Object*> childObjects;

    Object(Mesh* mesh, Material* material, Shader* shader)
        : mesh(mesh), material(material), shader(shader) { material->setup(shader); }

    std::function<void(float)> tickFunction;
    virtual void tick(float deltaTime) { if (tickFunction) tickFunction(deltaTime); }

    void draw(Camera* camera, std::vector<Light*>& lights);
    void draw(Camera* camera, Transform instanceTransform, std::vector<Light*>& lights);

    void addChild(Object* child) { childObjects.push_back(child); }

    void attachTo(Object* newParent) {
        newParent->addChild(this);
        parent = newParent;
    }

    virtual ~Object() {
        delete mesh;
        delete shader;
    }

    friend bool operator==(const Object& lhs, const Object& rhs) {
        return lhs.mesh == rhs.mesh
            && lhs.shader == rhs.shader;
    }

    friend bool operator!=(const Object& lhs, const Object& rhs) { return !(lhs == rhs); }
};
