﻿#pragma once
#include <vector>
#include <glad/glad.h>
#include <glm/vec3.hpp>

#include "Camera.hpp"
#include "Shader.h"
#include "Transform.hpp"
#include "assimp/mesh.h"


struct BoundingBox {
    glm::vec3 min, max;

    BoundingBox(const glm::vec3& min, const glm::vec3& max) {
        this->min = min;
        this->max = max;
    }
};

class Mesh : public Transform {
public:
    aiMesh* mesh;
    std::vector<unsigned int> indices;
    std::vector<glm::vec3> vertexNormals;

    GLuint VAO;
    GLuint VBO[4];
    GLuint EBO;

    Mesh(aiMesh* const meshData);
    BoundingBox getBoundingBox();
    void applyTransform(Camera* camera, Transform instanceTransform, Shader* shader);
    void normalise();

    ~Mesh() {
        glDeleteBuffers(3, VBO);
        glDeleteBuffers(1, &EBO);
        glDeleteVertexArrays(1, &VAO);
    }
};
