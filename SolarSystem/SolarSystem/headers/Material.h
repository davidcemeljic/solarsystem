﻿#pragma once
#include <vector>
#include <glm/vec3.hpp>

#include "Shader.h"
#include "assimp/material.h"

struct Texture2D {
    GLuint ID;
    int width = 0, height = 0, channels = 0;
    unsigned char* data = nullptr;
    aiTextureType type = aiTextureType_DIFFUSE;
};

class Material {
    aiMaterial* material;
    std::vector<Texture2D> textures;
public:
    explicit Material(aiMaterial* const material, std::string& scenePath)
        : material(material) {
        std::string dirPath(scenePath, 0, scenePath.find_last_of("\\/"));
        loadTextures(dirPath.append("\\"));
    }

    void applyTextures();
    void setup(Shader* shader);

    std::string getName() const;
    glm::vec3 getAmbientColor() const;
    glm::vec3 getDiffuseColor() const;
    glm::vec3 getSpecularColor() const;
    glm::vec3 getEmissiveColor() const;
    float getShininess() const;
    float getOpacity() const;

private:
    void loadTextures(const std::string& texturePath);
    void generateTexture(Shader* shader, Texture2D& texture);
};
