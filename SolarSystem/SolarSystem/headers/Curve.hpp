﻿#pragma once

#include <iostream>
#include <vector>

#include "Camera.hpp"
#include "Shader.h"
#include "Transform.hpp"

class Curve : public Transform {
    std::vector<glm::vec3> controlPoints;

    GLuint VAO_ControlPolygon;
    GLuint VBO_ControlPolygon;

    GLuint VAO_ApproxCurve;
    GLuint VBO_ApproxCurve;

    GLuint VAO_InterpCurve;
    GLuint VBO_InterpCurve;

    Shader* lineShader;
    Shader* approxShader;
    Shader* interpShader;

    int precision = 40;

    std::vector<glm::vec3> approxPoints;
    
    std::vector<glm::vec3> interpPoints;
    std::vector<glm::vec3> interpVectors;

public:
    Curve(const std::vector<glm::vec3>& control_points, Shader* lineShader, Shader* approxShader,
          Shader* interpShader);


    void draw(Camera* camera);
    void addControlPoint(glm::vec3 point);

    ~Curve() {
        glDeleteBuffers(1, &VBO_ControlPolygon);
        glDeleteVertexArrays(1, &VAO_ControlPolygon);

        glDeleteBuffers(1, &VBO_ApproxCurve);
        glDeleteVertexArrays(1, &VAO_ApproxCurve);

        glDeleteBuffers(1, &VBO_InterpCurve);
        glDeleteVertexArrays(1, &VAO_InterpCurve);
    }

    glm::vec3 getApproxPoint(float t);

    glm::vec3 getInterpPoint(float t);
    glm::vec3 getInterpTangent(float t);


private:
    void initControlPolygon();
    void initApproxCurve();
    void initInterpCurve();

    void drawControlPolygon(Camera* camera);
    void drawApproxCurve(Camera* camera);
    void drawInterpCurve(Camera* camera);

    void calculateInterpVectors();

    void calculateApproxCurve();
    void calculateInterpCurve();
    
    float f(int i, int n, float t);
    float b(int i, int n, float t);
    int binomialCoeff(int n, int k);
    int fact(int n);
};
