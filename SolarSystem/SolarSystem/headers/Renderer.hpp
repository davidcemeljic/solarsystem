﻿#pragma once

#include "Camera.hpp"
#include "Scene.hpp"

class Renderer {
    Camera* camera;
    Scene* scene;
    
public:
    Renderer(Scene* scene, Camera* camera);
    
    void render() const;

    ~Renderer();
};
